module.exports = (client, message, query, tracks) => {
    message.channel.send({
        embed: {
            color: '#00A551',
            title: 'Zoekresultaten',
            URL: 'https://minecraft.scouting.nl/',
            author: { name: `Hier zijn je zoekresultaten voor: ${query}` },
            timestamp: new Date(),
            description: `${tracks.map((t, i) => `**${i + 1}** - ${t.title}`).join('\n')}`,
        },
    });
};