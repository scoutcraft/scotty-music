const Discord = require('discord.js')
module.exports = (client, message, queue, playlist) => {
        const EmbedStart = new Discord.MessageEmbed()
            .setColor('#00A551')
        	.setTitle(`${playlist.title}`)
        	.setDescription(`Deze afspeellijst van **${playlist.tracks.length}** nummers is toegevoegd aan de wachtrij`)
        	.setURL('https://minecraft.scouting.nl/')
        	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	.setTimestamp();

        message.channel.send(EmbedStart);
};