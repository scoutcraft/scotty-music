const Discord = require('discord.js')
module.exports = (client, message, query, tracks) => {
        const EmbedStart = new Discord.MessageEmbed()
            .setColor('#fc9303')
        	.setTitle(`Geen juiste reactie`)
        	.setDescription(`Je hebt geen goede reactie gegeven. Stuur het commando nog een keer`)
        	.setURL('https://minecraft.scouting.nl/')
        	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	.setTimestamp();

        message.channel.send(EmbedStart);
};