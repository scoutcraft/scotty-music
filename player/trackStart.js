const Discord = require('discord.js')
module.exports = (client, message, track) => {
  const EmbedStart = new Discord.MessageEmbed()
    .setColor('#00A551')
    .setAuthor(`Artiest: ${track.author}`)
    .setTitle(`${client.emotes.music} - ${track.title}`)
    .setDescription(`**${track.requestedBy.username}** speelt muziek in **${message.guild.me.voice.channel}**`)
    .setURL('https://minecraft.scouting.nl/')
    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
    .setTimestamp();
  message.channel.send(EmbedStart);
  //als een nieuw liedje begint wordt de stemmers array gecleared ivm de voteskip
  stemmers = [];
};
