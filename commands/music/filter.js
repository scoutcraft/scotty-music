const Discord = require('discord.js');
module.exports = {
    name: 'filter',
    aliases: [],
    category: 'Leiding',
    utilisation: '{prefix}filter [filter name]',

    execute(client, message, args) {
        if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
          // has one of the roles
          const EmbedNotInChannelSkip = new Discord.MessageEmbed()
                      .setColor('#e71837')
                  	.setTitle(`Je zit niet in een spraakkanaal`)
                  	.setDescription(`Als je een filter wil toevoegen moet je een spraakkanaal joinen`)
                  	.setURL('https://minecraft.scouting.nl/')
                  	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                  	.setTimestamp();
          if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannelSkip);


          const EmbedNoSongInQueueSkip = new Discord.MessageEmbed()
                      .setColor('#fc9303')
                  	.setTitle(`De wachtrij is leeg!`)
                  	.setDescription(`Je kan nog geen filters toevoegen. Voeg eerst liedjes toe met
                         **/play [nummer]**`)
                  	.setURL('https://minecraft.scouting.nl/')
                  	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                  	.setTimestamp();
          if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueueSkip);


          const EmbedNotInSameChannelSkip = new Discord.MessageEmbed()
                      .setColor('#e71837')
                  	.setTitle(`Je zit niet in het goede spraakkanaal`)
                  	.setDescription(`Als je filters wil toevoegen moet je in **${message.guild.me.voice.channel}** zitten`)
                  	.setURL('https://minecraft.scouting.nl/')
                  	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                  	.setTimestamp();
          if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannelSkip);


            const filterToUpdate = client.filters.find((x) => x.toLowerCase() === args[0].toLowerCase());


            const EmbedNoGoodFilter = new Discord.MessageEmbed()
                .setColor('#e71837')
                .setTitle(`Dit is geen werkend filter`)
                .setDescription(`Om alle beschikbare filters te checken: **/filters**`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            if (!filterToUpdate) return message.channel.send(EmbedNoGoodFilter);

            const filtersUpdated = {};
            filtersUpdated[filterToUpdate] = client.player.getQueue(message).filters[filterToUpdate] ? false : true;
            client.player.setFilters(message, filtersUpdated);

            const EmbedFilterDisabled = new Discord.MessageEmbed()
                .setColor('#00A551')
                .setTitle(`Filter verwijderd`)
                .setDescription(`Het **${filterToUpdate}** filter wordt verwijderd van de muziek, dit kan even duren voor lange nummers`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            const EmbedFilterEnabled = new Discord.MessageEmbed()
                .setColor('#00A551')
                .setTitle(`Filter toegevoegd door de leiding`)
                .setDescription(`Ik voeg het **${filterToUpdate}** filter toe aan de muziek, dit kan even duren voor lange nummers`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            if (filtersUpdated[filterToUpdate]) message.channel.send(EmbedFilterEnabled);
            else message.channel.send(EmbedFilterDisabled);
        } else {
        message.channel.send({
                        embed: {
                            color: '#e71837',
                            author: { name: 'Geen rechten' },
                            timestamp: new Date(),
                            description: `Je hebt de rechten niet om **/filter** uit te mogen voeren`,
                        },
                    })};          // has none of the roles
    },
};
