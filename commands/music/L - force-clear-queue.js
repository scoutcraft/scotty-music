const Discord = require('discord.js');
module.exports = {
    name: 'forceclear',
    aliases: ['fclear'],
    category: 'Leiding',
    utilisation: '{prefix}forceclear',

    execute(client, message, track) {
      if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
        	.setTitle(`Je zit niet in een spraakkanaal`)
        	.setDescription(`Speel muziek door een in een spraakkanaal te stappen en
        	   **/play [nummer]** te typen`)
        	.setURL('https://minecraft.scouting.nl/')
        	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`De wachtrij is leeg, je kan dus niks verwijderen**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je de wachtrij wil verwijderen moet je in het volgende spraakkanaal zitten: **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedOnlyOneSong = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Er zit maar 1 liedje in de wachtrij`)
            .setDescription(`Je kan alleen **/forceclear** doen om de wachtrij te legen. als je muziek wil stoppen moet je **/forcestop** doen.`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (client.player.getQueue(message).tracks.length <= 1) return message.channel.send(EmbedOnlyOneSong);

        client.player.clearQueue(message);
        const EmbedClear = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`De wachtrij is verwijderd door de leiding`)
            .setDescription(`Alle nummers zijn verwijderd uit de wachtrij.`)
            .addFields({
                            name: `Wil je de muziek helemaal stoppen?`,
                            value: `doe dan **/forcestop**`,
                            inline: true
                        },
                        {
                            name: `Wil je de muziek toevoegen?`,
                            value: `doe dan **/play [nummer]**`,
                            inline: true
                        })
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        message.channel.send(EmbedClear);
      } else {
      message.channel.send({
        embed: {
          title: 'Geen rechten',
          url: 'https://minecraft.scouting.nl/',
          color: '#e71837',
          timestamp: new Date(),
          description: `Je hebt de rechten niet om **/forceclear** uit te mogen voeren`,
        },
      })};
    },
};
