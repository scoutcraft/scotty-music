const Discord = require('discord.js')
module.exports = {
    name: 'forceskip',
    aliases: ['fsk'],
    category: 'Leiding',
    utilisation: '{prefix}forceskip',

    execute(client, message, track) {
      if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
        const EmbedNotInChannelSkip = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in een spraakkanaal`)
                	.setDescription(`Als je een liedje wil overslaan moet je een spraakkanaal joinen`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannelSkip);


        const EmbedNoSongInQueueSkip = new Discord.MessageEmbed()
                    .setColor('#fc9303')
                	.setTitle(`De wachtrij is leeg!`)
                	.setDescription(`Je kan nog geen liedjes overslaan. Voeg eerst liedjes toe met
                       **/play [nummer]**`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueueSkip);


        const EmbedNotInSameChannelSkip = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in het goede spraakkanaal`)
                	.setDescription(`Als je een liedje wil overslaan moet je in **${message.guild.me.voice.channel}** zitten`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannelSkip);


        const EmbedSkip = new Discord.MessageEmbed()
                    .setColor('#00A551')
                	.setTitle(`Het liedje is overgeslagen door de leiding`)
                	.setDescription(`Het volgende nummer in de wachtrij wordt zo afgespeeld`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        client.player.skip(message);
        message.channel.send(EmbedSkip);
      } else {
      message.channel.send({
        embed: {
          title: 'Geen rechten',
          url: 'https://minecraft.scouting.nl/',
          color: '#e71837',
          timestamp: new Date(),
          description: `Je hebt de rechten niet om **/forceskip** uit te mogen voeren`,
        },
      })};
    },
};
