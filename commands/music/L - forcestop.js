const Discord = require('discord.js');
module.exports = {
    name: 'forcestop',
    aliases: ['fstop'],
    category: 'Leiding',
    utilisation: '{prefix}forcestop',

    execute(client, message, track) {
      if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in een spraakkanaal`)
            .setDescription(`Om de muziek te stoppen moet je in een spraakkanaal zitten`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Om de muziek te stoppen moet je in het volgende spraakkanaal zitten: **${message.guild.me.voice.channel}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedNoSongPlaying = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`Er speelt geen muziek`)
            .setDescription(`Er speelt nog geen muziek. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongPlaying);


        client.player.setRepeatMode(message, false);
        client.player.stop(message);


        const EmbedClear = new Discord.MessageEmbed()
            .setColor('##00A551')
            .setTitle(`De muziek is gestopt door de leiding`)
            .setDescription(`Wil je nieuwe muziek toevoegen? typ dan:
               **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        message.channel.send(EmbedClear);
      } else {
      message.channel.send({
        embed: {
          title: 'Geen rechten',
          url: 'https://minecraft.scouting.nl/',
          color: '#e71837',
          timestamp: new Date(),
          description: `Je hebt de rechten niet om **/forcestop** uit te mogen voeren`,
        },
      })};
    },
};
